package com.example.appblak_on

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.fragment_pesan.*
import kotlinx.android.synthetic.main.fragment_pesan.view.*
import mumayank.com.airlocationlibrary.AirLocation
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FragmentPesan : Fragment(), OnMapReadyCallback, View.OnClickListener {

    var airLoc: AirLocation? = null
    var gMap: GoogleMap? = null
    lateinit var mapFragment: SupportMapFragment
    lateinit var thisParent: MainActivity2

    val COLLECTION = "pemesanan"
    lateinit var storage: StorageReference
    lateinit var db1 : FirebaseFirestore
    lateinit var db: CollectionReference
    lateinit var alFile: ArrayList<HashMap<String, Any>>
    lateinit var adapter: CustomAdapter2
    lateinit var uri: Uri
    val F_ID = "id"
    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_HARGA = "file_harga"
    val F_URL = "file_url"
    val F_ID1 = "id"
    val F_NAMA1 = "nama"
    val F_MENU1 = "menu"
    val F_LOKASI1 = "lokasi"
    val F_HARGA1 = "harga"
    val RC_OK = 100
    var fileType = ""
    var fileName = ""
    var fileId = ""
    var docName = ""
    var docId = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_pesan, container, false)

        thisParent = activity as MainActivity2
        root.fab.setOnClickListener(this)
        root.btnTampil.setOnClickListener(this)
        root.btnTutup.setOnClickListener(this)

        mapFragment = childFragmentManager.findFragmentById(R.id.fragment1) as SupportMapFragment
        mapFragment.getMapAsync(this)

        root.lsMenu.setOnItemClickListener(itemClick)
        alFile = ArrayList()
        uri = Uri.EMPTY

        return root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLoc?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)

        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)) {
            if (data != null) {
                uri = data.data!!
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        airLoc?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onMapReady(p0: GoogleMap?) {
        gMap = p0
        if (gMap != null) {
            airLoc = AirLocation(thisParent, true, true,
                object : AirLocation.Callbacks {
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(
                            thisParent,
                            "Gagal mendapatkan posisi saat ini",
                            Toast.LENGTH_SHORT
                        ).show()
                        txLoc.setText("Gagal mendapatkan posisi saat ini")
                    }

                    override fun onSuccess(location: Location) {
                        val ll = LatLng(location.latitude, location.longitude)
                        gMap!!.addMarker(MarkerOptions().position(ll).title("Posisi Saya"))
                        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, 16.0f))
                        txLoc.setText(
                            "LAT=${location.latitude}, " +
                                    "LNG=${location.longitude}"
                        )
                    }

                })
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alFile.get(position)
        docId = hm.get(F_ID).toString()
        docName = hm.get(F_NAME).toString()
        var harga = hm.get(F_HARGA).toString()
        txNamaMenu.setText(docName)
        txHarga.setText(harga)
    }

    override fun onStart() {
        super.onStart()

        db1 = FirebaseFirestore.getInstance()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("menu")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException != null) {
                firebaseFireStoreException.message?.let { Log.e("Firestore : ", it) }
            }
            showData()
        }
    }

    fun showData() {
        db.get().addOnSuccessListener { result ->
            alFile.clear()
            for (doc in result) {
                val hm = HashMap<String, Any>()
                hm.put(F_ID, doc.get(F_ID).toString())
                hm.put(F_TYPE, doc.get(F_TYPE).toString())
                hm.put(F_NAME, doc.get(F_NAME).toString())
                hm.put(F_HARGA, doc.get(F_HARGA).toString())
                hm.put(F_URL, doc.get(F_URL).toString())
                alFile.add(hm)
            }
            adapter = CustomAdapter2(thisParent, alFile)
            lsMenu.adapter = adapter
        }
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.btnTampil->{
                lsMenu.setVisibility(View.VISIBLE)
            }
            R.id.btnTutup->{
                lsMenu.setVisibility(View.GONE)
            }
            R.id.fab->{
                val hm = HashMap<String, Any>()
                fileId = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                var harga = txHarga.text.toString().toInt()
                var jml = edJml.text.toString().toInt()
                hm.set(F_ID,fileId)
                hm.set(F_NAMA1,edNama.text.toString())
                hm.set(F_MENU1,txNamaMenu.text.toString())
                hm.set(F_HARGA1,harga*jml)
                hm.set(F_LOKASI1,txLoc.text.toString())
                db1.collection(COLLECTION).document(fileId).set(hm).addOnSuccessListener {
                    Toast.makeText(thisParent, txNamaMenu.text.toString()+" Berhasil Dipesan", Toast.LENGTH_SHORT).show()
                }.addOnFailureListener { e ->
                    Toast.makeText(thisParent,"Data gagal ditambah : ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}