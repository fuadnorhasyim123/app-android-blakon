package com.example.appblak_on.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.appblak_on.MainActivity
import com.example.appblak_on.R

class FragmentAbout : Fragment() {

    lateinit var thisParent: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        val root = inflater.inflate(R.layout.fragment_about, container, false)

        return root
    }
}