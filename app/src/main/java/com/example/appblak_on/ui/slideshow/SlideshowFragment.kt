package com.example.appblak_on.ui.slideshow

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.appblak_on.CustomAdapter
import com.example.appblak_on.MainActivity
import com.example.appblak_on.R
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.fragment_slideshow.*
import kotlinx.android.synthetic.main.fragment_slideshow.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class SlideshowFragment : Fragment(), View.OnClickListener {

    lateinit var thisParent: MainActivity
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter: CustomAdapter
    lateinit var uri : Uri
    val F_ID = "id"
    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val F_HARGA = "file_harga"
    val RC_OK = 100
    var fileType =""
    var fileHarga =""
    var fileName =""
    var fileId =""
    var docName = ""
    var docId = ""

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        val root = inflater.inflate(R.layout.fragment_slideshow, container, false)
        root.btnUpImg.setOnClickListener(this)
        root.btnUpload.setOnClickListener(this)
        root.btnDelete.setOnClickListener(this)
        root.alV.setOnItemClickListener(itemClick)
        alFile = ArrayList()
        uri = Uri.EMPTY

        return root
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alFile.get(position)
        docId = hm.get(F_ID).toString()
        docName = hm.get(F_NAME).toString()
        var extensi = hm.get(F_HARGA).toString()
        var pesan = "Menu : $docName Rp. $extensi akan di Hapus!"
        SelectedFile.setText(pesan)
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("menu")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException!=null){
                firebaseFireStoreException.message?.let{ Log.e("Firestore : ",it)}
            }
            showData()
        }
    }
    fun showData(){
        db.get().addOnSuccessListener { result ->
            alFile.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.put(F_ID, doc.get(F_ID).toString())
                hm.put(F_TYPE, doc.get(F_TYPE).toString())
                hm.put(F_HARGA, doc.get(F_HARGA).toString())
                hm.put(F_NAME, doc.get(F_NAME).toString())
                hm.put(F_URL, doc.get(F_URL).toString())
                alFile.add(hm)
            }
            adapter = CustomAdapter(thisParent,alFile)
            alV.adapter = adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null) {
                uri = data.data!!
                SelectedFile.setText(uri.toString())
            }
        }
    }

    override fun onClick(p0: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(p0?.id){
            R.id.btnUpImg ->{
                fileType = ".jpg"
                intent.setType("image/*")}
            R.id.btnUpload ->{
                if (uri != null){
                    fileId = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    fileName = txNama.text.toString()
                    fileHarga = txMenu.text.toString()
                    val fileRef = storage.child(fileName+fileType)
                    fileRef.putFile(uri)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener { taks ->
                            val  hm = HashMap<String,Any>()
                            hm.put(F_ID, fileId)
                            hm.put(F_NAME, fileName)
                            hm.put(F_TYPE,fileType)
                            hm.put(F_HARGA,fileHarga)
                            hm.put(F_URL, taks.result.toString())
                            db.document(fileName).set(hm).addOnSuccessListener {
                                Toast.makeText(
                                    thisParent,
                                    "File Telah Terupload",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                }
            }
            R.id.btnDelete ->{
                val fileRef = storage.child(fileName+fileType)
                val desertRef = storage.child(fileName+fileType)
                desertRef.delete().addOnSuccessListener {
                }.addOnFailureListener {
                }

                db.whereEqualTo("id",docId).get()
                    .addOnSuccessListener {results ->
                        for (doc in results){
                            db.document(doc.id).delete()
                                .addOnSuccessListener {
                                    Toast.makeText(thisParent,"Data Berhasil Dihapus", Toast.LENGTH_SHORT)
                                        .show() }
                                .addOnFailureListener { e ->
                                    Toast.makeText(thisParent,"Data Gagal Dihapus : ${e.message}",
                                        Toast.LENGTH_SHORT)
                                        .show() }
                        }
                    }.addOnFailureListener { e ->
                        Toast.makeText(thisParent,"Tidak dapat mengambil data : ${e.message}", Toast.LENGTH_SHORT)
                            .show() }
            }
        }
        if(p0?.id == R.id.btnUpImg ) startActivityForResult(intent,RC_OK) //untuk bisa membuka explorer di hp
    }
}