package com.example.appblak_on.ui.gallery

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.appblak_on.MainActivity
import com.example.appblak_on.R
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_gallery.*
import kotlinx.android.synthetic.main.fragment_gallery.edJml
import kotlinx.android.synthetic.main.fragment_gallery.edNama
import kotlinx.android.synthetic.main.fragment_gallery.view.*
import kotlinx.android.synthetic.main.fragment_pesan.*

class GalleryFragment : Fragment(), View.OnClickListener {

    val arrayMenu = arrayOf("Seblak Sosis","Seblak Kerupuk Ceker","Seblak Makaroni Pedas","Seblak Basah","Seblak Mie")
    lateinit var adapterSpin : ArrayAdapter<String>
    val COLLECTION = "pemesanan"
    val F_ID = "id"
    val F_NAMA = "nama"
    val F_MENU = "menu"
    val F_LOKASI = "lokasi"
    val F_HARGA = "harga"
    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alCustomer : ArrayList<HashMap<String, Any>>
    lateinit var adapter : SimpleAdapter
    lateinit var thisParent: MainActivity

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)
        thisParent = activity as MainActivity
        alCustomer = ArrayList()
        root.btnTambah.setOnClickListener(this)
        root.btnEdit.setOnClickListener(this)
        root.btnHapus.setOnClickListener(this)
        root.lsCustomer.setOnItemClickListener(itemClick)

        adapterSpin = ArrayAdapter(thisParent, android.R.layout.simple_list_item_1,arrayMenu)
        root.spMenu.adapter = adapterSpin
        return root
    }
    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if(e != null) e.message?.let { Log.d("firestore", it) }
            showData()
        }
    }
    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.btnTambah -> {
                val menu = adapterSpin.getItem(spMenu.selectedItemPosition)
                var harga = edHarga.text.toString().toInt()
                var jml = edJml.text.toString().toInt()
                val hm = HashMap<String, Any>()
                hm.set(F_ID,edId.text.toString())
                hm.set(F_NAMA,edNama.text.toString())
                if (menu != null) {
                    hm.set(F_MENU,menu)
                }
                hm.set(F_HARGA,harga*jml)
                hm.set(F_LOKASI,edLoc.text.toString())
                db.collection(COLLECTION).document(edId.text.toString()).set(hm).addOnSuccessListener {
                    Toast.makeText(thisParent, "Data berhasil ditambah", Toast.LENGTH_SHORT).show()
                }.addOnFailureListener { e ->
                    Toast.makeText(thisParent,"Data gagal ditambah : ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btnEdit -> {
                val hm = HashMap<String, Any>()
                var harga = edHarga.text.toString().toInt()
                var jml = edJml.text.toString().toInt()
                hm.set(F_ID,docId)
                hm.set(F_NAMA,edNama.text.toString())
                val jk1 = adapterSpin.getItem(spMenu.selectedItemPosition)
                if (jk1 != null) {
                    hm.set(F_MENU,jk1)
                }
                hm.set(F_HARGA,harga*jml)
                hm.set(F_LOKASI,edLoc.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(thisParent,"Data berhasil diedit", Toast.LENGTH_SHORT).show()
                    }.addOnFailureListener { e ->
                        Toast.makeText(thisParent,"Data gagal diedit : ${e.message}", Toast.LENGTH_SHORT).show()
                    }
            }
            R.id.btnHapus -> {
                db.collection(COLLECTION).whereEqualTo(F_ID,docId).get().addOnSuccessListener {
                        results ->
                    for(doc in results){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(thisParent, "Data Berhasil Dihapus", Toast.LENGTH_SHORT).show()
                            }.addOnFailureListener { e ->
                                Toast.makeText(thisParent, "Data Gagal Dihapus : ${e.message}", Toast.LENGTH_SHORT).show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(thisParent,"Tidak mendapatkan data ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alCustomer.get(position)
        docId = hm.get(F_ID).toString()
        edId.setText(docId)
        edNama.setText(hm.get(F_NAMA).toString())
        edHarga.setText(hm.get(F_HARGA).toString())
        edLoc.setText(hm.get(F_LOKASI).toString())
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alCustomer.clear()
            for(doc in result){
                val hm = HashMap<String, Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAMA,doc.get(F_NAMA).toString())
                hm.set(F_MENU,doc.get(F_MENU).toString())
                hm.set(F_HARGA,doc.get(F_HARGA).toString())
                hm.set(F_LOKASI,doc.get(F_LOKASI).toString())
                alCustomer.add(hm)
            }
            adapter = SimpleAdapter(thisParent, alCustomer,R.layout.row_data,
                arrayOf(F_ID,F_NAMA,F_MENU,F_HARGA,F_LOKASI),
                intArrayOf(R.id.txId,R.id.txNama, R.id.txMenu, R.id.txHarga,R.id.txLokasi))
            lsCustomer.adapter = adapter
        }
    }
}