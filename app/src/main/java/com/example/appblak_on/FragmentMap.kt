package com.example.appblak_on

import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import kotlinx.android.synthetic.main.fragment_map.*
import kotlinx.android.synthetic.main.fragment_map.txLoc
import kotlinx.android.synthetic.main.fragment_map.view.*
import kotlinx.android.synthetic.main.fragment_pesan.*
import mumayank.com.airlocationlibrary.AirLocation

class FragmentMap : Fragment(), OnMapReadyCallback, View.OnClickListener{

    lateinit var thisParent: MainActivity2
    val TOKO_1 = "prambon+nganjuk+jawa+timur"
    val TOKO_2 = "tarokan+kediri+jawa+timur"
    val MAPBOX_TOKEN = "pk.eyJ1IjoiZnV1IiwiYSI6ImNrZzY3YW80ejBvNG0ycXBpN2MxZjJ6MGEifQ.ae1ux1tcbOFYLRj7mA-uWw"
    var URL = ""

    var lat : Double = 0.0; var lng : Double = 0.0
    var airLoc : AirLocation? = null
    var gMap : GoogleMap? = null
    lateinit var mapFragment : SupportMapFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_map, container, false)
        thisParent = activity as MainActivity2

        mapFragment = childFragmentManager.findFragmentById(R.id.fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        root.fab.setOnClickListener(this)
        root.btnKeToko1.setOnClickListener(this)
        root.btnKeToko2.setOnClickListener(this)

        return root
    }

    fun getDestinationLoc(url: String){
        val request = JsonObjectRequest(Request.Method.GET,url,null,
            Response.Listener{
                val features = it.getJSONArray("features").getJSONObject(0)
                val place_name = features.getString("place_name")
                val center =features.getJSONArray("center")
                val lat = center.get(0).toString()
                val lng = center.get(1).toString()
                getDestinationRoutes(lng,lat,place_name)
            }, Response.ErrorListener{
                Toast.makeText(thisParent,"Tidak Mendapat Lokasi Yang Dituju", Toast.LENGTH_SHORT).show()
            })
        val q = Volley.newRequestQueue(thisParent)
        q.add(request)
    }

    fun getDestinationRoutes(destLat : String, destLng : String, place_name : String){
        URL = "https://api.mapbox.com/directions/v5/mapbox/driving/"+
                "$lng,$lat;$destLng,$destLat?access_token=$MAPBOX_TOKEN&geometries=geojson"
        val request = JsonObjectRequest(Request.Method.GET,URL,null,
            Response.Listener{
                val routes = it.getJSONArray("routes").getJSONObject(0)
                val legs = routes.getJSONArray("legs").getJSONObject(0)
                val distance =legs.getInt("distance")
                val duration = legs.getInt("duration")
                txLoc.setText("Lokasi Saya :\nLat : $lat  Lng : $lng\n"+
                        "Destination : $place_name\nLat : $destLat  Lng : $destLng\n"+
                        "Distance : $distance km    Duration : $duration minute")
                val geometry = routes.getJSONObject("geometry")
                val coordinates = geometry.getJSONArray("coordinates")
                val arraySteps = ArrayList<LatLng>()
                for(i in 0..coordinates.length()-1){
                    val lnglat = coordinates.getJSONArray(i)
                    val fLng = lnglat.getDouble(0)
                    val fLat = lnglat.getDouble(1)
                    arraySteps.add(LatLng(fLat,fLng))
                }
                drawRoutes(arraySteps,place_name)
            }, Response.ErrorListener{
                Toast.makeText(thisParent,"Ada yang salah! ${it.message.toString()}", Toast.LENGTH_SHORT).show()
            })
        val q = Volley.newRequestQueue(thisParent)
        q.add(request)
    }

    fun drawRoutes(array : ArrayList<LatLng>, place_name: String){
        gMap?.clear()
        val polyline = PolylineOptions().color(Color.BLUE).width(10.0f).clickable(true).addAll(array)
        gMap?.addPolyline(polyline)
        val ll= LatLng(lat,lng)
        gMap?.addMarker(MarkerOptions().position(ll).title("Posisi Saya"))
        gMap?.addMarker(MarkerOptions().position(array.get(array.size-1)).title(place_name))
        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,10.0f))
    }

    override fun onMapReady(p0: GoogleMap?) {
        gMap = p0
        if(gMap!=null){
            airLoc = AirLocation(thisParent,true,true,
                object : AirLocation.Callbacks{
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(thisParent,"Gagal mendapatkan posisi saat ini", Toast.LENGTH_SHORT).show()
                        txLoc.setText("Gagal mendapatkan posisi saat ini")
                    }

                    override fun onSuccess(location: Location) {
                        lat = location.latitude; lng = location.longitude
                        val ll = LatLng(location.latitude,location.longitude)
                        gMap!!.addMarker(MarkerOptions().position(ll).title("Posisi Saya"))
                        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                        txLoc.setText("Posisi Saya : LAT=${location.latitude}, " +
                                "LNG=${location.longitude}")
                    }

                })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLoc?.onActivityResult(requestCode,resultCode,data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        airLoc?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.btnKeToko1->{
                URL = "https://api.mapbox.com/geocoding/v5/mapbox.places/"+
                        "$TOKO_1.json?proximity=$lng,$lat&access_token=$MAPBOX_TOKEN&limit=1"
                getDestinationLoc(URL)
            }
            R.id.btnKeToko2->{
                URL = "https://api.mapbox.com/geocoding/v5/mapbox.places/"+
                        "$TOKO_2.json?proximity=$lng,$lat&access_token=$MAPBOX_TOKEN&limit=1"
                getDestinationLoc(URL)
            }
            R.id.fab->{
                val ll = LatLng(lat,lng)
                gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                txLoc.setText("Lokasi Saya : LAT=$lat, LNG=$lng")
            }
        }
    }

}