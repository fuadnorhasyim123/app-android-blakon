package com.example.appblak_on

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class CustomAdapter2(val context: Context,
                    arrayList : ArrayList<HashMap<String, Any>>) : BaseAdapter(){
    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val F_HARGA = "file_harga"
    val list = arrayList
    var uri = Uri.EMPTY

    inner class ViewHolder(){
        var txId : TextView? = null
        var txFileName : TextView? = null
        var txFileType : TextView? = null
        var txFileURL : TextView? = null
        var txFileHarga : TextView? = null
        var imv : ImageView? = null
    }
    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return  list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view:View? = convertView
        if(convertView == null) {
            var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_data2, null, true)

            holder.txId = view!!.findViewById(R.id.txFileName) as TextView
            holder.txFileName = view!!.findViewById(R.id.txFileName) as TextView
            holder.txFileHarga = view!!.findViewById(R.id.txFileHarga) as TextView
            holder.imv = view!!.findViewById(R.id.imv) as ImageView

            view.tag = holder
        }else{
            holder = view!!.tag as ViewHolder
        }

        var fileType:String = list.get(position).get(F_TYPE).toString()
        uri = Uri.parse(list.get(position).get(F_URL).toString())

        holder.txFileName!!.setText(list.get(position).get(F_NAME).toString())
        holder.txFileHarga!!.setText("Rp. "+list.get(position).get(F_HARGA).toString())

        when(fileType){
            ".jpg" -> {Picasso.get().load(uri).into(holder.imv)}
        }

        return view!!
    }
}
