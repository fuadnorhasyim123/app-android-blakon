package com.example.appblak_on

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.SupportMapFragment
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import kotlinx.android.synthetic.main.fragment_map.view.*

class FragmentDashboard : Fragment(), View.OnClickListener {

    lateinit var thisParent: MainActivity2

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        thisParent = activity as MainActivity2
        root.btnLogout.setOnClickListener(this)

        return root
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.btnLogout->{
                Toast.makeText(thisParent, "Berhasil Logout", Toast.LENGTH_SHORT).show()
                val intent = Intent(thisParent, LoginActivity::class.java)
                startActivity(intent)
            }
        }
    }
}